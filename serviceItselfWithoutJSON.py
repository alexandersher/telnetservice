import time

table = {}
inputedStr = raw_input('>')
words = inputedStr.split(' ')
while (words[0] != 'close') and (words[0] != 'exit') and (words[0] != 'quit'):
    if words[0] == 'put':
        nonSpaceWords = 0
        for item in words:
            if item != '':
                nonSpaceWords += 1
        
        if nonSpaceWords < 3:
             print 'error: too few arguments \nput <username> <flag>'
        if nonSpaceWords > 3:
             print 'error: too many arguments \nput <username> <flag>'
        if nonSpaceWords == 3:
            username = ''
            flag = ''
            for item in words:
                if item != '' and item != 'put' and username == '':
                    username = item
                if username != '' and item != '':
                    flag = item
            
            flag = flag.lower()
            if not flag.isalnum() or len(flag) != 4:
                if not flag.isalnum():
                    print 'error: non-allowed chars in flag, digits and letters only'
                if len(flag) != 4:
                    print 'error: flag is 4 chars long'
            else:
                tm = time.localtime()
                timeStr = str(tm[3]) + ':' + str(tm[4]) + ':' + str(tm[5]) + ' ' + str(tm[1]) + '.' + str(tm[2]) + '.' + str(tm[0])
                
                isExsist = False
                for item in table:
                    if table[item][1] == username:
                        print 'error: such username already exsists'
                        isExsist = True
                    if table[item][2] == flag:
                        print 'error: such flag already exsists'
                        isExsist = True
                if not isExsist:        
                    table[len(table) + 1] = [timeStr, username, flag]
                    print table
                    print 'done'
    
    if words[0] == 'find':
        nonSpaceWords = 0
        for item in words:
            if item != '':
                nonSpaceWords += 1
    
        if nonSpaceWords < 3:
            print 'error: too few arguments \nfind [-i <id> | -u <username> | -f <flag>]'
        if nonSpaceWords > 3:
            print 'error: too many arguments \nfind [-i <id> | -u <username> | -f <flag>]'
        if nonSpaceWords == 3:
            par = ''
            arg = ''
            for item in words:
                if item != '' and item != 'find' and par == '':
                    par = item
                if par != '' and item != '':
                    arg = item
                    
            if par == '-i' or par == '-u' or par == '-f':
                if par == '-i':
                    if not arg.isdigit():
                        print 'error: wrong argument, only numbers allowed'
                    else:
                        if table[int(arg)]:
                            print 'id = ' + arg + ' date = ' + str(table[int(arg)][0]) + ' username = ' + str(table[int(arg)][1]) + ' flag = ' + str(table[int(arg)][2])
                        else:
                            print 'error: nothing was found'
                if par == '-u':
                    isExsist = False
                    for item in table:
                        if  table[item][1] == arg:
                            print 'id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2])
                            isExsist = True
                            break
                    if not isExsist:
                        print 'error: nothing was found'
                if par == '-f':
                    isExsist = False
                    for item in table:
                        if  table[item][2] == arg:
                            print 'id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2])
                            isExsist = True
                            break
                    if not isExsist:
                        print 'error: nothing was found'
                
            else:
                print 'error: wrong parameter \nfind [-i <id> | -u <username> | -f <flag>]'
    if words[0] == 'list':
        for item in table:
            print 'id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2])
    if words[0] != 'put' and words[0] != 'find' and words[0] != 'list':
        print 'error: unknown command'
    inputedStr = raw_input('>')
    words = inputedStr.split(' ')