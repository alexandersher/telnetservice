import socket
import select
import time

if __name__ == '__main__':
    connectionList = []
    recvBuffer = 4096
    port = 5000
    table = {}
    
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverSocket.bind(("0.0.0.0", port))
    serverSocket.listen(10)
    
    connectionList.append(serverSocket)
    while 1:
        readSockets, writeSockets, errorSockets = select.select(connectionList,[],[])
 
        for sock in readSockets:
            
            if sock == serversocket:
                sockfd, addr = serverSocket.accept()
                connectionList.append(sockfd)
            else:
                try:
                    sock.send("                             ______ \n                         ___/_____/\ \n                        /         /\\ \n                  _____/__       /  \\ \n                _/       /\_____/___ \  \n               //       /  \       /\ \  \n       _______//_______/    \     / _\/______ \n      /      / \       \    /    / /        /\ \n   __/      /   \       \  /    / /        / _\__\n  / /      /     \_______\/    / /        / /   /\ \n /_/______/___________________/ /________/ /___/  \ \n \ \      \    ___________    \ \        \ \   \  / \n  \_\      \  /          /\    \ \        \ \___\/ \n     \      \/   Yozhik   /  \    \ \        \  / \n      \_____/          /    \    \ \________\/ \n           /__________/      \    \  / \n           \   _____  \      /_____\/ \n            \ /    /\  \    /___\/ \n             /____/  \  \  / \n             \    \  /___\/ \n              \____\/ \n\n")
                    sock.send('>')
                    inputedStr = sock.recv(recvBuffer)
                    words = inputedStr.split(' ')
                    while (words[0] != 'close') and (words[0] != 'exit') and (words[0] != 'quit'):
                        if words[0] == 'put':
                            nonSpaceWords = 0
                            for item in words:
                                if item != '':
                                    nonSpaceWords += 1
        
                            if nonSpaceWords < 3:
                                sock.send('error: too few arguments \nput <username> <flag>')
                            if nonSpaceWords > 3:
                                sock.send('error: too many arguments \nput <username> <flag>')
                            if nonSpaceWords == 3:
                                username = ''
                                flag = ''
                                for item in words:
                                    if item != '' and item != 'put' and username == '':
                                        username = item
                                    if username != '' and item != '':
                                        flag = item
            
                                flag = flag.lower()
                                if not flag.isalnum() or len(flag) != 4:
                                    if not flag.isalnum():
                                        sock.send('error: non-allowed chars in flag, digits and letters only')
                                    if len(flag) != 4:
                                        sock.send('error: flag is 4 chars long')
                                else:
                                    tm = time.localtime()
                                    timeStr = str(tm[3]) + ':' + str(tm[4]) + ':' + str(tm[5]) + ' ' + str(tm[1]) + '.' + str(tm[2]) + '.' + str(tm[0])
                
                                    isExsist = False
                                    for item in table:
                                        if table[item][1] == username:
                                            sock.send('error: such username already exsists')
                                            isExsist = True
                                        if table[item][2] == flag:
                                            sock.send('error: such flag already exsists')
                                            isExsist = True
                                    if not isExsist:        
                                        table[len(table) + 1] = [timeStr, username, flag]
                                        sock.send('done')
    
                        if words[0] == 'find':
                            nonSpaceWords = 0
                            for item in words:
                                if item != '':
                                    nonSpaceWords += 1
    
                            if nonSpaceWords < 3:
                                sock.send('error: too few arguments \nfind [-i <id> | -u <username> | -f <flag>]')
                            if nonSpaceWords > 3:
                                sock.send('error: too many arguments \nfind [-i <id> | -u <username> | -f <flag>]')
                            if nonSpaceWords == 3:
                                par = ''
                                arg = ''
                                for item in words:
                                    if item != '' and item != 'find' and par == '':
                                        par = item
                                    if par != '' and item != '':
                                        arg = item
                    
                                if par == '-i' or par == '-u' or par == '-f':
                                    if par == '-i':
                                        if not arg.isdigit():
                                            sock.send('error: wrong argument, only numbers allowed')
                                        else:
                                            if table[int(arg)]:
                                                sock.send('id = ' + arg + ' date = ' + str(table[int(arg)][0]) + ' username = ' + str(table[int(arg)][1]) + ' flag = ' + str(table[int(arg)][2]))
                                            else:
                                                sock.send('error: nothing was found')
                                    if par == '-u':
                                        isExsist = False
                                        for item in table:
                                            if  table[item][1] == arg:
                                                sock.send('id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2]))
                                                isExsist = True
                                                break
                                        if not isExsist:
                                            sock.send('error: nothing was found')
                                    if par == '-f':
                                        isExsist = False
                                        for item in table:
                                            if  table[item][2] == arg:
                                                sock.send('id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2]))
                                                isExsist = True
                                                break
                                        if not isExsist:
                                            sock.send('error: nothing was found')
                
                                else:
                                    sock.send('error: wrong parameter \nfind [-i <id> | -u <username> | -f <flag>]')
                        if words[0] == 'list':
                            for item in table:
                                sock.send('id = ' + str(item) + ' date = ' + str(table[item][0]) + ' username = ' + str(table[item][1]) + ' flag = ' + str(table[item][2]))
                        if words[0] != 'put' and words[0] != 'find' and words[0] != 'list':
                            sock.send('error: unknown command')
                        sock.send('>')    
                        inputedStr = sock.recv(recvBuffer)
                        words = inputedStr.split(' ')    
                except:
                    sock.close()
                    connectionList.remove(sock)
                    continue
     
    serverSocket.close()   